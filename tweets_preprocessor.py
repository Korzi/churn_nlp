import numpy as np
from sklearn.preprocessing import LabelEncoder
from keras.preprocessing import sequence
from nlp_utils import tokenize


class TweetsPreprocessor:
    TWEET_TO_REMOVE_VALUE = -1

    def __init__(self, X, y, word2vec_model, word2vec_size):
        self.X = np.copy(X)
        self.y = np.copy(y)
        self.word2vec_model = word2vec_model
        self.word2vec_size = word2vec_size
        self.max_words_in_tweet = -1

    def get_max_words_in_tweet(self):
        return self.max_words_in_tweet

    def preprocess_data(self):
        self.encode_labels()
        self.tokenize()
        self.filter_not_available_words()
        self.vectorize()
        self.add_zero_padding()
        self.reshape_examples()
        return self.X, self.y

    def encode_labels(self):
        print('Encoding labels...')
        labelencoder = LabelEncoder()
        self.y = labelencoder.fit_transform(self.y).reshape(-1, 1)

    def tokenize(self):
        print('Tokenizing data...')
        self.X = list(tokenize(self.X))

    def filter_not_available_words(self):
        print('Filtering out not available words...')
        for idx, tw in enumerate(self.X):
            self.X[idx] = list(filter(lambda x: x in self.word2vec_model.vocab, tw))
            if len(self.X[idx]) == 0:
                self.y[idx] = self.TWEET_TO_REMOVE_VALUE
        self._remove_empty_tweets()
        self.max_words_in_tweet = np.max(list(map(lambda line: len(line), self.X)))
        print('max words in tweet = ' + str(self.max_words_in_tweet))

    def _remove_empty_tweets(self):
         self.X = list(filter(lambda line: len(line) > 0, self.X))
         self.y = np.reshape(list(filter(lambda x: x > self.TWEET_TO_REMOVE_VALUE, self.y)), (-1, 1))

    def vectorize(self):
        print('Vectorizing...')
        self.X = [self.word2vec_model[tweet] for tweet in self.X]

    def add_zero_padding(self):
        print('Zero padding...')
        X_padded = np.zeros(shape=(len(self.X), self.max_words_in_tweet, self.word2vec_size))
        for i in range(len(self.X)):
            X_padded[i, :self.X[i].shape[0], :self.X[i].shape[1]] = self.X[i]
        self.X = X_padded

    def reshape_examples(self):
        print('Reshaping...')
        self.X = np.reshape(self.X, (len(self.X), self.max_words_in_tweet, self.word2vec_size))

    def return_examples(self):
        return self.X, self.y
