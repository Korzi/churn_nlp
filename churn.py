import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn.metrics import roc_auc_score
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.wrappers.scikit_learn import KerasClassifier
from common_utils import get_folds, assert_correct_label_encoding
from telco_preprocessor import TelcoPreprocessor


def remove_columns_with_redundant(dataset):
    # "charges" columns depends on "mins" columns - redundancy
    redundant_chrage_columns = ['Day Charge', 'Eve Charge', 'Night Charge', 'Intl Charge']
    data_without_redundant_columns = dataset.drop(redundant_chrage_columns, axis=1)
    return remove_columns(data_without_redundant_columns)

def remove_columns(dataset, columns = ['Unnamed: 0', 'State', 'Phone']):
    # index, state, phone
    return dataset.drop(columns, axis=1)


# Importing the dataset and removing not needed columns
dataset = pd.read_csv('churn.csv')
X = remove_columns_with_redundant(dataset).iloc[:, :-1]
y = dataset.iloc[:, -1]
telco_preprocessor = TelcoPreprocessor(X, y)

# data preprocessing
X, y = telco_preprocessor.perform_basic_preprocessing()
assert_correct_label_encoding(y, true_index=10, false_index=0)

# init ANN
print('Initializing ANN...')
BATCH_SIZE = 190
classifier = Sequential()
classifier.add(Dense(units=36, kernel_initializer='uniform', activation='relu', input_dim=17))
classifier.add(Dropout(rate=0.1))
classifier.add(Dense(units=1, kernel_initializer='uniform', activation='sigmoid'))
classifier.compile(optimizer = 'rmsprop', loss = 'binary_crossentropy', metrics = ['accuracy'])

# cross-validation training
'''set test size > 0 if you want test on unseen data'''
x_tr, x_ts, y_tr, y_ts = train_test_split(X, y, test_size=0.0)

y_pred = np.zeros_like(y_tr, dtype=float)
folds, examples_in_batch = get_folds(x_tr, y_tr)
index = 0
print(classifier.summary())
for (X_train, y_train, X_test, y_test) in folds:
    print('\n\nstart fold number ' + str(index + 1))
    classifier.fit(X_train, y_train, batch_size=BATCH_SIZE, epochs=100, shuffle=True)
    start = index * examples_in_batch
    y_pred[start:start + examples_in_batch] = classifier.predict_proba(X_test, batch_size=BATCH_SIZE)
    index+= 1

auc_score = roc_auc_score(y_tr, y_pred)
print('auc = ' + str(auc_score))

# evaluate
if len(x_ts) > 0:
    loss, acc = classifier.evaluate(x_ts, y_ts, batch_size=BATCH_SIZE)
    print('Test loss / test accuracy = {:.4f} / {:.4f}'.format(loss, acc))
