## Required libraries:
numpy, pandas, sklearn, tensorflow, keras, gensim (for NLP)


## Word2Vec model
To run tweets.py you also need to download GoogleNews-vectors-negative300.bin.gz from :
https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/
Then you need to unpack it to project directory.


## Commands to run:
churn task: python churn.py
nlp task: python tweets.py


## Additional info:
Data processing in NLP task may take some time due to loading word2vec model. 
If you run script in IDE like Spyder (where you can save variables in workspace), load word2vec model once and comment that line - you will not waste time for loading when you run script again.