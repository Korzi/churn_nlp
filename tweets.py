import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout
import warnings
warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')
from gensim.models.keyedvectors import KeyedVectors
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
from common_utils import get_folds, assert_correct_label_encoding
from tweets_preprocessor import TweetsPreprocessor


# import training set
print('Loading data...')
data = pd.read_csv('sentiment.tsv', sep='\t', header=None)
X = data.iloc[:, 1]
y = data.iloc[:, 0]

# data preprocessing
print('Data preprocessing...')
w2v_model = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)
WORD2VEC_SIZE = 300

tweets_preprocessor = TweetsPreprocessor(X, y, w2v_model, WORD2VEC_SIZE)
X, y = tweets_preprocessor.preprocess_data()
max_words_in_tweet = tweets_preprocessor.get_max_words_in_tweet()
assert_correct_label_encoding(y, true_index=3, false_index=0)

# init RNN
print('Initializing RNN...')
BATCH_SIZE = 50
classifier = Sequential()
classifier.add(LSTM(10, input_shape=(max_words_in_tweet, WORD2VEC_SIZE)))
classifier.add(Dense(units=1, activation='sigmoid'))
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# cross-validation training
'''set test size > 0 if you want test on unseen data'''
x_tr, x_ts, y_tr, y_ts = train_test_split(X, y, test_size=0.0)

folds, examples_in_batch = get_folds(x_tr, y_tr)
y_pred = np.zeros_like(y_tr, dtype=float)
index = 0
print(classifier.summary())
for (X_train, y_train, X_test, y_test) in folds:
    print('\n\nstart fold number ' + str(index + 1))
    classifier.fit(X_train, y_train, batch_size=BATCH_SIZE, epochs=4, shuffle=True)
    start = index * examples_in_batch
    y_pred[start:start + examples_in_batch] = classifier.predict_proba(X_test)
    index+= 1

tweets_auc = roc_auc_score(y_tr, y_pred)
print('\nauc = ' + str(tweets_auc))

# evaluate
if len(x_ts) > 0:
    loss, acc = classifier.evaluate(x_ts, y_ts, batch_size=BATCH_SIZE)
    print('Test loss / test accuracy = {:.4f} / {:.4f}'.format(loss, acc))
