import nltk
from nltk.tokenize import TweetTokenizer

nltk.download('punkt', quiet=True)
nltk.download('stopwords', quiet=True)

from nltk.corpus import stopwords

regexp_tokenizer = nltk.RegexpTokenizer("[\w']+")
tweet_tokenizer = TweetTokenizer(strip_handles=True, reduce_len=True)
stopwords = set(stopwords.words('english'))

tokenizers = [
    lambda x: tweet_tokenizer.tokenize(x), # special for tweets (remove )
    lambda x: [regexp_tokenizer.tokenize(tok) for tok in x],  # splits over whitespaces and removes interpunction
    lambda x: [tok for sent in x for tok in sent],  # flat list
    lambda x: [t.lower() for t in x],  # changes to lower
    lambda x: [word for word in x if word not in stopwords]  # filter stopwords
]


def tokenize(tweets):
    for tokenizer in tokenizers:
        tweets = map(tokenizer, tweets)
    return tweets
