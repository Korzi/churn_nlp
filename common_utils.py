import numpy as np
from sklearn.preprocessing import LabelEncoder


def assert_correct_label_encoding(y, true_index, false_index):
    # checking if encoded labels are: false = 0, true = 1
    if len(y) < (max(true_index, false_index) + 1):
        raise ValueError('Too short dataset. Should be min 11 values')
    if y[false_index] != 0 or y[true_index] != 1:
        raise ValueError('Wrong encoding. False != 0 or True != 1')

def get_folds(X_data, y_data, k = 10):
    examples_in_batch = int(len(X_data) / k)
    folds = []
    for i in range(10):
        start = i*examples_in_batch
        x_test = X_data[start: (start + examples_in_batch), :]
        y_test = y_data[start: (start + examples_in_batch), :]
        x_beg = X_data[:start, :]
        x_end = X_data[(start + examples_in_batch):, :]
        x_tr = concatenate_arrays(x_beg, x_end)
        y_beg = y_data[:start, :]
        y_end = y_data[(start + examples_in_batch):, :]
        y_tr = concatenate_arrays(y_beg, y_end).reshape(-1, 1)
        folds.append((x_tr, y_tr.flatten(), x_test, y_test.reshape(-1, 1)))
    return folds, examples_in_batch

def concatenate_arrays(begin, end):
    if begin.size:
        return np.vstack([begin, end]) if end.size else begin
    if end.size:
        return end
    raise ValueError('No data to vstack')

def categorize(dataset, column_name):
    tmp = dataset.copy()
    labelencoder = LabelEncoder()
    tmp[column_name] = labelencoder.fit_transform(tmp[column_name])
    return tmp