import numpy as np
from sklearn.preprocessing import OneHotEncoder, Imputer, StandardScaler
from common_utils import categorize

class TelcoPreprocessor:
    TWEET_TO_REMOVE_VALUE = -1

    def __init__(self, X, y):
        self.X = X
        self.y = y
        self.sc = StandardScaler()

    def perform_basic_preprocessing(self):
        self._encode_labels()
        self._clean_vmail()
        self._fill_unknown_number_columns_with_strategy(['Eve Calls', 'Eve Mins', 'Intl Calls'], 'median')
        self._categorize_multiclass_variables()
        self._standarize()
        return self.X, self.y

    def _encode_labels(self):
        self.y = categorize(self.y.to_frame(), 'Churn?').iloc[:, 0].values.reshape(-1, 1)

    def _clean_vmail(self):
        self.X['VMail Plan'] = np.where(self.X['VMail Message'] > 0, 'yes', 'no')
        self.X = categorize(self.X, 'VMail Plan')

    def _categorize_multiclass_variables(self):
        self.X = categorize(self.X, 'Area Code')
        self.X = categorize(self.X, 'Int\'l Plan')
        self._one_hot_encode(1) # one hot Area Code
        self._one_hot_encode(4) # one hot Intl Plan

    def _one_hot_encode(self, column_index):
        ohencoder = OneHotEncoder(categorical_features = [column_index]) 
        self.X = ohencoder.fit_transform(self.X).toarray()
        self.X = self.X[:, 1:] # remove one column to avoid dummy variable trap

    def _standarize(self):
        self.X = self.sc.fit_transform(self.X)

    def _fill_unknown_number_columns_with_strategy(self, columns, strategy, unknown_value='?', unique_missing_value_val=-1):
        '''unique_missing_value_val - numeric value that doesn't exist in column (to avoid override correct values)'''
        for column in columns:
            self.X[column] = self.X[column].replace(unknown_value, unique_missing_value_val)
            self.X[column] = self.X[column].apply(lambda x: float(x))
            imputer = Imputer(missing_values = unique_missing_value_val, strategy = strategy, axis = 0)
            imputer.fit(self.X.loc[:, column].values.reshape(-1, 1))
            self.X.loc[:, column] = imputer.transform(self.X.loc[:, column].values.reshape(-1, 1))

    def fill_unknown_charges(ds, charge_columns=['Day', 'Night', 'Intl']):
        '''to check results with this column - not used currently'''
        for col in charge_columns:
            ds[col + ' Charge'] = ds[col + ' Charge'].replace('?', -1)
            ds[col + ' Charge'] = ds[col + ' Charge'].apply(lambda x: float(x))
            price = float(ds[col + ' Charge'][3]) / float(ds[col + ' Mins'][3])
            ds[col + ' Charge'] = np.where(ds[col + ' Charge'] == -1, ds[col + ' Mins'] * price, ds[col + ' Charge'])
        return ds